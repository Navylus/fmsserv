'use strict'
let mongoose = require('mongoose')
let Schema = mongoose.Schema

let MovesNormalModel = new Schema({
  nameCharacter: {
    type: String,
    required: 'Required'
  },
  moveName: {
    type: String,
    required: 'Required'
  },
  plnCmd: {
    type: String
  },
  numCmd: {
    type: String
  },
  startup: {
    type: String
  },
  active: {
    type: String
  },
  recovery: {
    type: String
  },
  onHit: {
    type: String
  },
  onBlock: {
    type: String
  },
  airmove: {
    type: String
  },
  moveType: {
    type: String
  },
  moveMotion: {
    type: String
  },
  moveButton: {
    type: String
  }
})

module.exports = mongoose.model('MovesNormal', MovesNormalModel)
