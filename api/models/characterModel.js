'use strict'
let mongoose = require('mongoose')
let Schema = mongoose.Schema


let CharacterSchema = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the character'
  },
  health: {
    type: String,
    required: 'Kindly enter the health of the character'
  },
  stun: {
    type: String,
    required: 'Kindly enter the stun of the character'
  },
  vgauge1: {
    type: String,
    required: 'Kindly enter the vgauge1 of the character'
  },
  vgauge2: {
    type: String,
    required: 'Kindly enter the vgauge2 of the character'
  },
  taunt: {
    type: String,
    required: 'Kindly enter the taunt of the character'
  },
  nJump: {
    type: String,
    required: 'Kindly enter the nJump of the character'
  },
  fJump: {
    type: String,
    required: 'Kindly enter the fJump of the character'
  },
  bJump: {
    type: String,
    required: 'Kindly enter the bJump of the character'
  },
  fDash: {
    type: String,
    required: 'Kindly enter the fDash of the character'
  },
  bDash: {
    type: String,
    required: 'Kindly enter the bDash of the character'
  },
  bDashCHFrames: {
    type: String,
    required: 'Kindly enter the bDashCHFrames of the character'
  },
  color: {
    type: String,
    required: 'Kindly enter the color of the character'
  },
  phrase: {
    type: String,
    required: 'Kindly enter the phrase of the character'
  },
  fWalk: {
    type: String,
    required: 'Kindly enter the fWalk of the character'
  },
  bWalk: {
    type: String,
    required: 'Kindly enter the bWalk of the character'
  },
  fJumpDist: {
    type: String,
    required: 'Kindly enter the fJumpDist of the character'
  },
  bJumpDist: {
    type: String,
    required: 'Kindly enter the bJumpDist of the character'
  },
  fDashDist: {
    type: String,
    required: 'Kindly enter the fDashDist of the character'
  },
  bDashDist: {
    type: String,
    required: 'Kindly enter the bDashDist of the character'
  },
  throwHurt: {
    type: String,
    required: 'Kindly enter the throwHurt of the character'
  },
  throwRange: {
    type: String,
    required: 'Kindly enter the throwRange of the character'
  }
})

module.exports = mongoose.model('Character', CharacterSchema)