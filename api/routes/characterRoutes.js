'use strict'
module.exports = function(app) {
  var characterList = require('../controllers/characterController')

  // characterList Routes
  app.route('/character/importFromJSON').get(characterList.importFromJSON)

  app.route('/character/listCharacter').get(characterList.listCharacter)

  app.route('/character/:name').get(characterList.findCharacter)
  app.route('/charactersDescription').get(characterList.desc)
}
