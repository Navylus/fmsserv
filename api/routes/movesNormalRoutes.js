'use strict'
module.exports = function(app) {
  var movesNormalList = require('../controllers/movesNormalController')

  // movesNormalList Routes
  app.route('/movesNormal/importFromJSON').get(movesNormalList.importFromJSON)

  app
    .route('/movesNormal/:nameCharacter')
    .get(movesNormalList.allMovesByCharacter)

  app.route('/movesNormal/byName/:moveName').get(movesNormalList.allMovesByName)

  app
    .route('/movesNormal/:nameCharacter/:moveName')
    .get(movesNormalList.findCharacterMoves)

  app
    .route('/movesNormal/byType/:nameCharacter/:moveType')
    .get(movesNormalList.findMovesByType)

  app
    .route('/movesNormal/airType/:nameCharacter/:airmove')
    .get(movesNormalList.findMovesAirType)

  app
    .route('/movesNormal/normals/:nameCharacter/:airmove')
    .get(movesNormalList.findNormalMoves)
}
