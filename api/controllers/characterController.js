'use strict'

const data = require('../../test.json')
const description = require('../../public/description.json')
const mongoose = require('mongoose'),
  Character = mongoose.model('Character')

exports.importFromJSON = function(req, res) {
  Character.remove({}, function(err) {
    console.log('collection removed')
  })
  for (var chara in data) {
    const charaStats = data[chara].stats
    const newChara = new Character(charaStats)
    newChara.save(function(err, chara) {
      if (err) {
        console.log(err)
        res.send(err)
      }
      console.log(chara)
    })
  }
  res.send('characters inserted.')
}

exports.listCharacter = function(req, res) {
  Character.find({}, function(err, chara) {
    if (err) {
      res.send(err)
    }
    res.json(chara)
  })
}

exports.findCharacter = function(req, res) {
  Character.find({ name: req.params.name }, null, function(err, chara) {
    if (err) {
      res.send(err)
    }
    console.log(chara)
    res.json(chara)
  })
}

exports.desc = function(req, res) {
  res.json(description)
}
