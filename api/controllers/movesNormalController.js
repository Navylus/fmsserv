'use strict'

const data = require('../../test.json')
const mongoose = require('mongoose')
const MovesNormal = mongoose.model('MovesNormal')

exports.importFromJSON = function(req, res) {
  MovesNormal.remove({}, function(err) {
    console.log('collection removed')
  })
  const allInfos = []
  for (let chara in data) {
    const name = data[chara].stats.name
    for (let move in data[chara].moves.normal) {
      const moveInfos = data[chara].moves.normal[move]
      const charaStats = {
        nameCharacter: name,
        moveName: moveInfos.moveName,
        plnCmd: moveInfos.plnCmd,
        numCmd: moveInfos.numCmd,
        startup: moveInfos.startup,
        active: moveInfos.active,
        recovery: moveInfos.recovery,
        onHit: moveInfos.onHit,
        onBlock: moveInfos.onBlock,
        airmove: moveInfos.airmove,
        moveType: moveInfos.moveType,
        moveMotion: moveInfos.moveMotion,
        moveButton: moveInfos.moveButton
      }
      const newMovesInfos = new MovesNormal(charaStats)
      newMovesInfos.save(function(err, infos) {
        if (err) {
          console.log(err)
          res.send(err)
        }
        console.log(infos)
      })
    }
  }
  res.setHeader('Content-Type', 'application/json')
  res.send(`normal moves inserted.`)
}

exports.allMovesByCharacter = function(req, res) {
  const { params } = req
  MovesNormal.find(
    {
      $query: { nameCharacter: params.nameCharacter },
      $orderby: { startup: 1 }
    },
    null,
    function(err, chara) {
      if (err) {
        res.send(err)
      }
      console.log(chara)
      res.json(chara)
    }
  )
}

exports.allMovesByName = function(req, res) {
  const { params } = req
  MovesNormal.find({ plnCmd: params.moveName }, null, function(err, move) {
    if (err) {
      res.send(err)
    }
    console.log(move)
    res.json(move)
  })
}

exports.findCharacterMoves = function(req, res) {
  const { params } = req
  MovesNormal.find(
    { plnCmd: params.moveName, nameCharacter: params.nameCharacter },
    null,
    function(err, move) {
      if (err) {
        res.send(err)
      }
      console.log(move)
      res.json(move)
    }
  )
}

exports.findMovesAirType = function(req, res) {
  const { params } = req
  MovesNormal.find(
    { airmove: params.airmove, nameCharacter: params.nameCharacter },
    null,
    function(err, move) {
      if (err) {
        res.send(err)
      }
      console.log(move)
      res.json(move)
    }
  )
}

// /movesNormal/normals/Abigail/false
exports.findNormalMoves = function(req, res) {
  const { params } = req
  MovesNormal.find(
    {
      airmove: params.airmove,
      nameCharacter: params.nameCharacter,
      moveType: 'normal'
    },
    null,
    function(err, move) {
      if (err) {
        res.send(err)
      }
      console.log(move)
      res.json(move)
    }
  )
}

exports.findMovesByType = function(req, res) {
  const { params } = req
  MovesNormal.find(
    { moveType: params.moveType, nameCharacter: params.nameCharacter },
    null,
    function(err, move) {
      if (err) {
        res.send(err)
      }
      console.log(move)
      res.json(move)
    }
  )
}
