const { createServer } = require('https')
const { readFileSync } = require('fs')
const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const helmet = require('helmet')

const TaskCharacter = require('./api/models/characterModel')
var routesCharacter = require('./api/routes/characterRoutes')
const TaskMovesNormal = require('./api/models/movesNormalModel')
var routesMovesNormal = require('./api/routes/movesNormalRoutes')

const port = process.env.PORT
const app = express()

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/FMSDB')

app.use(express.static('public'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
      imgSrc: ['*'],
      upgradeInsecureRequests: true
    }
  })
)

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  next()
})
app.use(helmet.frameguard({ action: 'deny' }))
app.use(helmet.noSniff())
app.use(helmet.ieNoOpen())
app.use(helmet.referrerPolicy({ policy: 'no-referrer' }))

routesMovesNormal(app)
routesCharacter(app)

createServer(
  {
    key: readFileSync(process.env.SSL_KEY),
    cert: readFileSync(process.env.SSL_CERT)
  },
  app
).listen(process.env.PORT)
